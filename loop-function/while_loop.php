<?php

/* 
 * Loops are used to execute the same block of code again and again, until a certain condition is met. 
 * The basic idea behind a loop is to automate the repetitive tasks within a program to save the time and effort.
 * while(condition){
    // Code to be executed
} 
 */

//The while loop executes a block of code while a condition is true.

$i=1;
while ($i<=3){ 
    
    echo 'The number is : '.$i."<br/>";
        $i++;
}
/*
 * $i hold the value=1, now check the condition while value of ($i<=5).
 *  it means it execute the code five times. it print the statement line by line. 
 * The number is : 1
The number is : 2
The number is : 3*/
?>

<hr>

<?php
/*The example below define a loop that starts with $i=1. 
The loop will continue to run as long as $i is less than or equal to 3. The $i will increase by 1 each time the loop runs:*/
$i=1;
while ($i<=5){
    $i++;
    echo 'This number is : '.$i."<br>";
}
/*This number is : 2
This number is : 3
This number is : 4
This number is : 5
This number is : 6*/

?>

<hr>

<?php

$i=1;
$sum=0;
while ($i<=100){
    $sum=$sum+$i;
    $i++;
}

echo 'Sum : '.$sum;

//Sum : 5050

?>