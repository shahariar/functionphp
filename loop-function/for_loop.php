<?php

/* 
 * The for loop repeats a block of code until a certain condition is met. 
 * It is typically used to execute a block of code for certain number of times.
 * for(initialization; condition; increment){
    // Code to be executed
} 
 */

