<?php

/* 
 * The switch-case statement is an alternative to the if-elseif-else statement
 *  The switch-case statement tests a variable against a series of values until it finds a match, 
 * and then executes the block of code corresponding to that match.
 * switch(n){
    case label1:
        // Code to be executed if n=label1
        break;
    case label2:
        // Code to be executed if n=label2
        break;
    ...
    default:
        // Code to be executed if n is different from all labels
} 
 */

$today=  date('D');
switch ($today){
    case 'Sat':
        echo 'Today is Saterday. Clean your house.';
        break;
    case 'Sun':
        echo 'Today is Sunday. Buy some food.';
        break;
    case 'Mon':
        echo 'Today is Monday.Visit a doctor.';
        break;
    case 'Tue':
        echo 'Today is Tuesday. Repair your car.';
        break;
    case 'Wed':
        echo 'Today is Wednesday.Its movie time.';
        break;
    case 'Tue':
        echo 'Today is Thursday.Do some rest.';
        break;
    case 'Fri':
        echo 'Today is Friday. Party tonight.';
        break;
    default :
        echo 'No information available for that day.';
        break;
    
}

?>