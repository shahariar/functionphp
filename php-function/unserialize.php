<?php

/* 
 The unserialize() converts to actual data from serialized data.
 */

$serialize_data=  serialize(array("Math","Language","Science"));
echo $serialize_data."<br>";
// Unserialize the data  
$unserialize_data=  unserialize($serialize_data);
// Show the unserialized data; 
var_dump($unserialize_data);
?>