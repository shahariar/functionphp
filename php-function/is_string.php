<?php

/* 
 The is_string () function is used to find whether a variable is a string or not.
 */

$name="Mostafijur";
if(is_string($name)){
    echo 'This is string..';
}  else {
    echo 'This is not string..';
}
echo '<br>';
var_dump("xyx");
echo '<br>';
var_dump(123);
echo '<br>';
var_dump(10.234);
