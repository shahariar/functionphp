<?php


/* 
 * The is_null () function is used to test whether a variable is NULL or not.  
So you can understand it better:

$a = null;
$b = 0;

is_null($a) // TRUE
$a == null  // TRUE
$a === null // TRUE
is_null($b) // FALSE
$b == null  // TRUE
$b === null // FALSE


 */


$a=null;
if(is_null($a)){
    echo "is null";
}  else {
    echo 'is not null'; 
}

?>
<hr>
Example-02

<?php

$b=1;
if(is_null($b)){
    echo 'is null';
}  else {
    echo 'is not null';
}



?>
<hr>
Example-03

<?php
$variable='';
if($variable==null){
    echo 'is null';
}
 else {
    echo 'is not null';    
}

?>

<hr>
Example-04

<?php
$x=null;
if($x===null){
    echo 'is null';
}
 else {
    echo 'is not null';    
}

?>
