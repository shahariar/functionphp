<?php

/* 
 The isset () function is used to check whether a variable is set or not.
 * The isset() function return false if testing variable contains a NULL value.
 * isset(variable1, variable2......)
 */

$valu="Hello World";
var_dump(isset($valu));
?>

<hr>

<?php
 $a=NULL;
 $b=FALSE; 
 $c="Learning isset";
 $d="";
 
 if(isset($a)){
     echo '$a'. "  is True" ;//$a is false
 }  else {
     echo '$a'."  is false";     
}
  echo '<br/>';
if(isset($b)){
     echo '$b'. "  is True" ;//$b is True
 }  else {
     echo '$b'."  is false";     
}
 echo '<br/>';
if(isset($c)){
     echo '$c'. "  is True" ;//$b is True
 }  else {
     echo '$c'."  is false";     
}
echo '<br/>';
if(isset($d)){
     echo '$d'. "  is True" ;//$b is True
 }  else {
     echo '$d'."  is false";     
}





?>

