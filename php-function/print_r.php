<?php

/* 
 The print_r() function is used to print human-readable information about a variable.
 * print_r(var_name, return_output)
 */

$a="abc";
print_r($a);
echo '<br>';
$b=11.23;
print_r($b);
echo '<br>';
$abc=array('s1'=>'physics','s2'=>'math','s3'=>'english','class'=>array(1,2,3,4,5));
print_r($abc);
?>