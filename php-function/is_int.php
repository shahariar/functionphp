<?php


/* The is_int () function is used to test whether the type of the specified variable is an integer or not.
 * Return value

TRUE if var_name is an integer, FALSE otherwise
 
 */

$data=array(123,"123",NULL,new stdClass,true,false);
foreach($data as $value){
    echo "is-int(";
    var_export($value);
    echo ")";
    
    var_dump(is_int($value));
}

?>
<hr>

<?php
$var_name1=123;
var_dump(is_int($var_name1));
$var_name2="abc";
var_dump(is_int($var_name2));

?>